import { Component, OnInit } from '@angular/core';
import { Countdown } from './countdown-widget/countdown.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  markers: { date: string; reached: boolean }[];
  endDate: Date;
  showTimer = false;
  endMessage: string = '';

  constructor() {
    this.markers = [];
  }
  ngOnInit() {}

  reciveFormValue(formValue: Countdown) {
    this.endDate = new Date();
    this.endDate.setHours(
      this.endDate.getHours() + +formValue.hours,
      this.endDate.getMinutes() + +formValue.minutes,
      this.endDate.getSeconds() + +formValue.seconds
    );
    this.showTimer = true;
    this.endMessage = '';
  }

  isReached(timeMarkers: { date: string; reached: boolean }[]) {
    timeMarkers.forEach((el) => {
      el.reached = true;
    });
  }
  endReached(endMessage: string) {
    this.showTimer = false;
    this.endMessage = endMessage;
  }

  handleTimeStamp($event: any) {
    let date = new Date();
    date.setHours(
      date.getHours() + +$event.hours,
      date.getMinutes() + +$event.minutes,
      date.getSeconds() + +$event.seconds
    );
    this.markers.push({
      date: `${$event.hours || 0}:${$event.minutes || 0}:${
        $event.seconds || 0
      }`,
      reached: false,
    });
    console.log($event);
  }
}
