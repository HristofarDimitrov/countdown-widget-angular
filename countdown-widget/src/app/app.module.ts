import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { CountdownWidgetComponent } from './countdown-widget/countdown-widget.component';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { CreateCountdownComponent } from './create-countdown/create-countdown.component';
import { CreateTimeStampFormComponent } from './create-time-stamp-form/create-time-stamp-form.component';

@NgModule({
  declarations: [
    AppComponent,
    CountdownWidgetComponent,
    CreateCountdownComponent,
    CreateTimeStampFormComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
