import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Countdown } from '../countdown-widget/countdown.interface';

@Component({
  selector: 'app-create-countdown',
  templateUrl: './create-countdown.component.html',
  // styleUrls: ['./create-countdown.component.css'],
})
export class CreateCountdownComponent implements OnInit {
  myForm: FormGroup;
  @Output() formValue = new EventEmitter<Countdown>();

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.myForm = this.fb.group({
      hours: ['', [Validators.min(0), Validators.pattern('[0-9]*')]],
      minutes: [
        '',
        [Validators.min(0), Validators.max(60), Validators.pattern('[0-9]*')],
      ],
      seconds: [
        '',
        [Validators.min(0), Validators.max(60), Validators.pattern('[0-9]*')],
      ],
    });
  }

  get hours() {
    return this.myForm.get('hours');
  }
  get minutes() {
    return this.myForm.get('minutes');
  }
  get seconds() {
    return this.myForm.get('seconds');
  }

  sendFormValue() {
    this.formValue.emit(this.myForm.value);
  }
}
