import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Countdown } from '../countdown-widget/countdown.interface';

@Component({
  selector: 'app-create-time-stamp-form',
  templateUrl: './create-time-stamp-form.component.html',
  // styleUrls: ['./create-time-stamp-form.component.css'],
})
export class CreateTimeStampFormComponent implements OnInit {
  myForm: FormGroup;
  @Output() formValue = new EventEmitter<Countdown>();

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.myForm = this.fb.group({
      hours: ['', [Validators.min(0), Validators.pattern('[0-9]*')]],
      minutes: [
        '',
        [Validators.min(0), Validators.max(60), Validators.pattern('[0-9]*')],
      ],
      seconds: [
        '',
        [Validators.min(0), Validators.max(60), Validators.pattern('[0-9]*')],
      ],
    });
  }

  get hours() {
    return this.myForm.get('hours');
  }
  get minutes() {
    return this.myForm.get('minutes');
  }
  get seconds() {
    return this.myForm.get('seconds');
  }

  sendFormValue() {
    console.log(this.myForm.value);

    this.formValue.emit(this.myForm.value);
  }
}
