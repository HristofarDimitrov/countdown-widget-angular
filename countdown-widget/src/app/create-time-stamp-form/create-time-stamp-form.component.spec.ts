import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateTimeStampFormComponent } from './create-time-stamp-form.component';

describe('CreateTimeStampFormComponent', () => {
  let component: CreateTimeStampFormComponent;
  let fixture: ComponentFixture<CreateTimeStampFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateTimeStampFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateTimeStampFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
