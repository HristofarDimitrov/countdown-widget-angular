import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { interval, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-countdown-widget',
  templateUrl: './countdown-widget.component.html',
  styleUrls: ['./countdown-widget.component.css'],
})
export class CountdownWidgetComponent implements OnInit {
  @Input()
  endDate: Date;
  @Input()
  timeMarkers: { date: string; reached: boolean }[];

  @Output()
  endReached: EventEmitter<null> = new EventEmitter();

  @Output()
  timeMarkerReached: EventEmitter<
    { date: string; reached: boolean }[]
  > = new EventEmitter();

  timer$: Observable<string>;

  ngOnInit(): void {
    this.timer$ = interval(1000).pipe(
      map(() => {
        const date = this.endDate.getTime() - new Date().getTime();
        const curDate = new Date();

        const existingTimeMarker = this.timeMarkers.filter(
          (item) => item.date === this.secondsToTime(date / 1000).timerToCompare
        );

        if (existingTimeMarker.length > 0) {
          this.timeMarkerReached.emit(existingTimeMarker);
        }

        if (date / 1000 <= 0) {
          this.endReached.emit();
        }
        const timer = this.secondsToTime(date / 1000).time;

        return `Hours: ${timer.hours} Minutes: ${timer.minutes} Seconds: ${timer.seconds}`;
      })
    );
  }

  private secondsToTime(
    secs: number
  ): {
    timerToCompare: string;
    time: { hours: number; minutes: number; seconds: number };
  } {
    const hours = Math.floor(secs / 3600);
    secs %= 3600;
    const minutes = Math.floor(secs / 60);
    const seconds = Math.ceil(secs % 60);
    return {
      timerToCompare: `${hours}:${minutes}:${seconds}`,
      time: {
        hours,
        minutes,
        seconds,
      },
    };
  }
}
