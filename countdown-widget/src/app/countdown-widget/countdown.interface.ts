export interface Countdown {
  hours: string;
  minutes: string;
  seconds: string;
}
